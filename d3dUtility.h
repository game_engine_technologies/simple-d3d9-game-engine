#pragma once
#include <Windows.h>

#include <d3dcompiler.h>
#include <d3d9.h>
#include <d3dx9.h>

#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <exception>
#include <sstream>
#include <list>
#include <filesystem>
#include <algorithm>
#include <iostream>

namespace fs = std::experimental::filesystem;

#pragma comment (lib,"d3d9.lib")
#pragma comment (lib,"d3dx9.lib")

namespace d3d9
{
	bool loadOBJ(IDirect3DDevice9* dev, IDirect3DVertexBuffer9** buf, const char * path);

	FLOAT gradus_to_radian(FLOAT gradus);

	struct D2VTexture
	{
		float u, v;
		D2VTexture() :u(0), v(0) {}
		D2VTexture(float u, float v) :u(u), v(v) {}
	};

	struct Vertex
	{
		float x, y, z;
		float nx, ny, nz;
		float u, v;
		static const DWORD FVF;
		Vertex():x(0), y(0), z(0), nx(0), ny(0), nz(0), u(0), v(0){}
		Vertex(float x, float y, float z, float nx, float ny, float nz, float u, float v) :x(x), y(y), z(z), nx(nx), ny(ny), nz(nz), u(u), v(v) {}
		Vertex(const D3DXVECTOR3& v, const D2VTexture& t, const D3DXVECTOR3& n) :x(v.x), y(v.y), z(v.z), u(t.u), v(t.v),
			nx(n.x), ny(n.y), nz(n.z) {};
		Vertex(const D3DXVECTOR3& v, const D3DXVECTOR2& t, const D3DXVECTOR3& n) :x(v.x), y(v.y), z(v.z), u(t.x), v(t.y),
			nx(n.x), ny(n.y), nz(n.z) {};
		friend std::ofstream& operator<<(std::ofstream& f, const Vertex& v);
	};

	struct Index
	{
		int v, t, n;
		Index() :v(0), t(0), n(0) {}
		Index(int v, int t, int n) :v(v), t(t), n(n) {}
	};

	class D3DXMaterial
	{
		D3DMATERIAL9 material;

	public:
		D3DXMaterial();
		D3DXMaterial(const D3DXCOLOR& c, const D3DXCOLOR& e, float p);
		D3DXMaterial(const D3DXCOLOR& a, const D3DXCOLOR& d, const D3DXCOLOR& s, const D3DXCOLOR& e, float p);
		D3DXMaterial(const D3DXMaterial& m);
		D3DXMaterial& operator=(const D3DXMaterial& m);
		D3DMATERIAL9 GetMaterial(); // если надо изменить какое-то из значений члена material
	};

	class Colors
	{
	public:
		// Colors
		static const D3DXCOLOR Red;
		static const D3DXCOLOR White;
		static const D3DXCOLOR Black;
		static const D3DXCOLOR Blue;
		static const D3DXCOLOR Grey;
		static const D3DXCOLOR Yellow;
		//
	};

	class D3DXLight
	{
		D3DLIGHT9 light;

	public:
		D3DXLight();
		D3DXLight(D3DLIGHTTYPE t, const D3DXCOLOR& d, const D3DXCOLOR& s, const D3DXCOLOR& a, const D3DXVECTOR3& v, float r);
		D3DXLight(D3DLIGHTTYPE t, const D3DXCOLOR& d, const D3DXCOLOR& s, const D3DXCOLOR& a, const D3DXVECTOR3& v);
		D3DXLight(const D3DXLight& l);
		D3DXLight& operator=(const D3DXLight& l);
		D3DLIGHT9& GetLight();
	};

	class D3DXTexture
	{
		IDirect3DTexture9* texture;

	public:
		D3DXTexture() :texture(nullptr) { }
		D3DXTexture(const D3DXTexture& t);
		D3DXTexture& operator=(const D3DXTexture& t);
		void FreeTexture(); // ручное удаление текстуры
		bool LoadFromFile(IDirect3DDevice9* dev, const char* name_file); // грузим текстуру из файла
		IDirect3DTexture9* GetTexture() { return this->texture; }
		~D3DXTexture();
	};

	class ErrorFile : public std::exception
	{
		std::string message;
	public:
		ErrorFile(const char* m) :message(m) {}
		const char* what()const;
	};

	class D3DModel
	{
		//std::vector<size_t> CountVertexBuffer; // размер каждого буфера вершин
		IDirect3DVertexBuffer9* buffer; // буфер вершин
		std::vector< std::pair< std::wstring, size_t > > vB; // буфер вершин
		std::map<std::wstring, D3DXTexture> textures; // буфер текстур
		std::map<std::wstring, D3DXMaterial> material; // буфер материалов
		std::wstring FileName;
	public:
		D3DModel(const wchar_t* fn) :FileName(fn), buffer(nullptr) {}
		void LoadModel(IDirect3DDevice9* dev); // грузим модель
											   // из файла
											   // генерирует исключение ErrorFile

		std::map<std::wstring, D3DXTexture> GetTexturesBuffer() { return this->textures; }
		std::map<std::wstring, D3DXMaterial> GetMaterialBuffer() { return this->material; }
		std::vector< std::pair< std::wstring, size_t > > GetVertexBufferData() { return this->vB; }
		IDirect3DVertexBuffer9* GetBuffer() { return this->buffer; }
		void SetBuffer(IDirect3DVertexBuffer9* vb) { this->buffer = vb; } // не реализвоаны проверки
		std::wstring GetFileName() { return this->FileName; }
		//std::vector<size_t> GetCountVertexBuffer() { return this->CountVertexBuffer; }
		void Free(); // принудительная очистка памяти
	};

	class D3DXDevice // класс устройство
	{
		IDirect3DDevice9* Device; // указатель на созданное устройство
		IDirect3D9* d3d9; // хранит все устройства, для вывода графики
		D3DPRESENT_PARAMETERS d3dpp; // // структура, которая хранит ряд характеристик устройства Direct3D
		int height; // высота окна
		int width; // ширина окна
		float RotateY; // угол поворота модели
		HWND hwnd; // дескриптор окна, кому принадлежит устройство
		std::vector<D3DXLight> lights; // все источники света
		std::vector<D3DModel> models; // буфер моделей
		std::list<fs::path> files; // имена моделей(пути до каждой)

		// синглетон
		D3DXDevice(const D3DXDevice& dxd);
		D3DXDevice& operator=(const D3DXDevice& dxd) ;

	public:
		static const float _2PI; // два Pi(360 град в радианах)
		D3DXDevice();
		bool InitD3D(HWND, int, int, bool, D3DDEVTYPE); // создаем устройство

		IDirect3DDevice9* getDevice(); // геттер получения устройства
		void EnableLight(bool f); // разрешаем свет
		void SetEnableLight(int n, bool f); // устанавливаем источник света и включаем его(+ ренормализация вершин и свечение)
		void SetMaterial(int num, const std::wstring& key); // устанавливаем текущий материал
		void SetTexturesFilter(IDirect3DTexture9* texture); // устанавливаем фильтры текстуры + отрисовываемую текстуру
		bool InitializationModel(); // метод инициализации модели(загружаем модель в память, вызвать в WM_CREATE)
		void PaintModel(); // отрисовка модели(-ей)
		void ReadModelsCatalog(const wchar_t* name); // читаем все модели из указанного каталога
		float GetRotateYAngle()const { return this->RotateY; }
		float& GetRotateYAngle() { return this->RotateY; }
		// генерирует исключение при неверном файле(пустое исключение)

		// void reverseScreen(HWND, bool, int); // разворачиваем на полный экран(и обратно)
		~D3DXDevice(); // деструктор
	};

};