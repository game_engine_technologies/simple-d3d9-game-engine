#pragma once

#include "d3dUtility.h"

#define WM_CREATEDX 111

class _window
{
	HWND hwnd;
	WNDCLASSEX wndclass;
	d3d9::D3DXDevice DXDevice;

public:
	_window(UINT style, int cbClsExtra,
		int cbWndExtra, HINSTANCE hInstance, HICON hIcon, HCURSOR hCursor,
		HBRUSH hbrBackground, LPCWSTR lpszMenuName, LPCWSTR lpszClassName, HICON hIconSm);

	bool create_win(const WCHAR* nameClass, const WCHAR* nameWin, DWORD style, int x, int y, int nW, int nH, HWND hPar, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam);
	MSG queue_message(int iCmdShow);
	HWND& getHWND();
	static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
	d3d9::D3DXDevice& GetDXDevice() { return this->DXDevice; }
	void RenderScene();
};