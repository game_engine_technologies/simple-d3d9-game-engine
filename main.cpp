#include "window.h"
#include <ctime>

//#pragma comment (lib,"winmm.lib")

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int iCmdShow)
{
	srand(time(nullptr)); 
	
	std::wstring szAppName = L"DirectX9"; // в основном - название класса окна и т.д.

	// объявляем окно
	_window _main((CS_HREDRAW | CS_VREDRAW), 0, 0, hInst, 
		LoadIcon(NULL, IDI_APPLICATION), LoadCursor(NULL, IDC_ARROW), (HBRUSH)GetStockObject(WHITE_BRUSH),
		NULL, szAppName.c_str(), LoadIcon(NULL, IDI_ASTERISK)); // создаём экземпляр класс окно

	// create window + dx device
	if (!_main.create_win(szAppName.c_str(), L"The DirectX Program", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 1000, 700, NULL, NULL, hInst, NULL)) 
		// регистрируем класс и создаём окно + устройство directX
		return 0; // в случае ошибки завершаем работу программы

	// поехали ловить сообщения
	WPARAM result(_main.queue_message(iCmdShow).wParam); // начинаем работу с очередью сообщений
	return result;
}