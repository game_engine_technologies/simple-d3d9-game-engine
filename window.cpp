#include "window.h"

_window::_window(UINT style/*, WNDPROC lpfnWndProc*/, int cbClsExtra, int cbWndExtra, HINSTANCE hInstance,
	HICON hIcon, HCURSOR hCursor, HBRUSH hbrBackground, LPCWSTR lpszMenuName, LPCWSTR lpszClassName, HICON hIconSm): DXDevice()
{
	this->wndclass.cbSize = sizeof(wndclass); // устанавливает размер класса окна в байтах. Всегда использовать sizeof(имя_экземпляра_класса)
	this->wndclass.style = CS_HREDRAW | CS_VREDRAW; // стиль класса, CS_HREDRAW - горизонтальный размер, CS_VREDRAW - вертикальный размер. Устанавливает, что при изменении данного размера будет осуществлена перерисовка окна
	this->wndclass.lpfnWndProc = this->WndProc; // указатель на оконную процедуру(события, которые будут происходить с окном)
	this->wndclass.cbClsExtra = 0; // число дополнительных байт(буфер)
	this->wndclass.cbWndExtra = 0; // число дополнительных байт(буфер)
	this->wndclass.hInstance = hInstance;  // устанавливает описатель экземпляра, переданный как аргумент WinMain
	this->wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); // загружает иконку для использования в программе(иконка в трее), NULL - значёк по-умолчанию, IDI_APPLICATION - без иконки
	this->wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); // загружает курсор
	this->wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); // получает графический объект(для фона окна), закрашивая в белый цвет
	this->wndclass.lpszMenuName = NULL; // верхнее меню. NULL - если его нет
	this->wndclass.lpszClassName = lpszClassName; // имя класса - присваиваем из названия окна
	this->wndclass.hIconSm = LoadIcon(NULL, IDI_ASTERISK); // иконка слева от заголовка программы

}

bool _window::create_win(const WCHAR * nameClass, const WCHAR * nameWin, DWORD style, int x, int y, int nW, int nH, HWND hPar, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
{
	WCHAR string[100] = { L"Error register class " };
	if (!RegisterClassEx(&wndclass))
	{
		wcsncat(string, const_cast<WCHAR*>(nameClass), wcslen(nameClass));
		MessageBox(0, string, L"Critical error", NULL);
		return false;
	}
	this->hwnd = CreateWindow( // создаёт окно
		nameClass, // имя класса окна
		nameWin, // заголовок окна
		style, // стиль окна
		x, // начальное положение x
		y, // начальное положение y
		nW, // начальный размер x
		nH, // начальный размер y
		hPar, // описатель родительского окна
		hMenu, // описатель меню окна
		hInstance, // описатель экземпляра программы
		lpParam // параметры создания
	);
	SetWindowLongPtr(this->hwnd, GWLP_USERDATA, (LONG_PTR)this); // дабы потом из глобальной процедуры обратиться к нему

	if (!this->hwnd)
	{
		string[0] = '\0';
		wcscpy_s(string, 100, L"Error create window ");
		wcsncat(string, const_cast<WCHAR*>(nameWin), wcslen(nameWin));
		MessageBox(0, string, L"Critical error", NULL);
		return false;
	}

	if (!this->DXDevice.InitD3D(this->hwnd, nW, nH, true, D3DDEVTYPE_HAL)) // создаем устройство dx
	{
		string[0] = '\0';
		wcscpy_s(string, 100, L"Error create dx-device window ");
		wcsncat(string, const_cast<WCHAR*>(nameWin), wcslen(nameWin));
		MessageBox(0, string, L"Critical error", NULL);
		return false;
	}

	PostMessage(this->hwnd, WM_CREATEDX, NULL, NULL);
	ShowWindow(this->hwnd, SW_SHOW); // показывает окно на экране, используя описатель окна + начальный вид окна(в трее и т.д.)
	UpdateWindow(this->hwnd); // перерисовывает область окна после его отрисовки

	return true;
}

MSG _window::queue_message(int iCmdShow)
{
	MSG msg;
	//PeekMessage(&msg, 0, 0, 0, PM_REMOVE); // получаем первое сообщение
	while (GetMessage(&msg, NULL, 0, 0)) // получает сообщение из очереди сообщений
	{
		TranslateMessage(&msg); // преобразует некоторые сообщения, полученные с помощью клавиатуры
		DispatchMessage(&msg); // отправляет сообщение оконной процедуре
	}
	return msg;
}

HWND& _window::getHWND()
{
	return this->hwnd;
}

LRESULT CALLBACK _window::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static RECT rc = { 0 };
	float angle(0.05f);
	HDC hdc;
	PAINTSTRUCT ps;
	_window *w = (_window *)GetWindowLongPtr(hwnd, GWLP_USERDATA); // получаем адрес класса, где все это добро лежит
	if (w)
	{
		switch (msg) // получили сообщение
		{
		case WM_CREATEDX: // сообщение о создании устройства dx
		{
			w->GetDXDevice().ReadModelsCatalog(L"model"); // ищем модели в каталоге
			w->GetDXDevice().InitializationModel(); // загружаем все модели + включаем все нужные режимы
			w->GetDXDevice().PaintModel(); // отрисовываем модель
			//ShowWindow(w->hwnd, SW_SHOW); 
			break;
		}

		case WM_KEYDOWN:
		{
			bool flag(false);
			switch (wParam)
			{
			case VK_LEFT:
				w->DXDevice.GetRotateYAngle() += angle;
				if (angle > d3d9::D3DXDevice::_2PI)
					angle = 0.f;
				flag = true;
				break;

			case VK_RIGHT:
				w->DXDevice.GetRotateYAngle() -= angle;
				if (angle < -d3d9::D3DXDevice::_2PI)
					angle = d3d9::D3DXDevice::_2PI;
				flag = true;
				break;

			default:
				break;
			}
			if (flag)
				w->GetDXDevice().PaintModel();
			break;
		}

		case WM_DESTROY: // закрытие процесса
		{
			PostQuitMessage(0);
			break;
		}


		}
	}
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

void _window::RenderScene()
{
	this->GetDXDevice().ReadModelsCatalog(L"model"); // ищем модели в каталоге
	this->GetDXDevice().InitializationModel(); // загружаем все модели + включаем все нужные режимы
	this->GetDXDevice().PaintModel(); // отрисовываем модель
}

// переделать потом перерисовку модели, только той ее части, что была стерта
// сделать мышкой приближение/отдаление камеры
// сделать вращение по x(ровное)
