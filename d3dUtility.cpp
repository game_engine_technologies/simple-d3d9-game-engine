#include "d3dUtility.h"

namespace d3d9
{
	D3DXDevice::D3DXDevice() :Device(nullptr), d3d9(nullptr), d3dpp(), height(0), width(0), RotateY(0.f){}

	const float D3DXDevice::_2PI(gradus_to_radian(360.));

	bool D3DXDevice::InitD3D(HWND wind, int width, int height, bool windowed, D3DDEVTYPE deviceType)
	{
		// дескриптор процесса
		// ширина окна
		// высота окна
		// true - если не в полный экран
		// тип устройства(физический или программный)
		// устройство, которое будет отвечать за отрисовку всего и вся

		this->hwnd = wind;
		this->height = height;
		this->width = width;

		// Init D3D: 
		int vp; // флаг для проверки, поддерживает ли видеокарта аппаратную обработку вершин

		// Step 1: Create the IDirect3D9 object.
		d3d9 = Direct3DCreate9(D3D_SDK_VERSION); // получаем все устройства (D3D_SDK_VERSION - всегда указывать данный аргумент)

		if (!d3d9) // если не удалось получить информацию о видеокарте
		{
			MessageBox(0, L"Direct3DCreate9() - FAILED", 0, 0); // печатаем сообщение об ошибке
			return false; // возвращаем неудачу в главную функцию
		}

		// Step 2: Check for hardware vp.
		D3DCAPS9 caps; // содержит информацию о первичном видеоадаптере(видеокарте по-умолчанию)
		d3d9->GetDeviceCaps(D3DADAPTER_DEFAULT, deviceType, &caps); // получаем информацию о первичном видеоадаптере
																	// D3DADAPTER_DEFAULT - первичный видеоадаптер, тип устройства(физический или программый, передаётся из главной функции), результат получения информации

		if (caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) // если логическое И даёт истину
			vp = D3DCREATE_HARDWARE_VERTEXPROCESSING; // устанавливаем флаг, в поддержку АППАРАТНОЙ обработки
		else
			vp = D3DCREATE_SOFTWARE_VERTEXPROCESSING; // устанавливаем флаг, в поддержку ПРОГРАММНОЙ обработки

		 // Step 3: Fill out the D3DPRESENT_PARAMETERS structure.
		d3dpp.BackBufferWidth = width; // ширина вторичного буффера в пикселях
		d3dpp.BackBufferHeight = height; // высота вторичного буффера в пикселях
		d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8; // формат пикселей
		d3dpp.BackBufferCount = 1; // количество вторичных буфферов
		d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE; // тип множественной выборки для вторичного буффера(в данный момент null)
		d3dpp.MultiSampleQuality = 0; // уровень качества множественной выборки(в данный момент null)
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD; // как будет осуществляться переключение буферов в цепочке переключений
		d3dpp.hDeviceWindow = this->hwnd; // дескиптор окна, на котором будет происходить отображение графики
		d3dpp.Windowed = windowed; // false, если в полноэкранном режиме
		d3dpp.EnableAutoDepthStencil = true; // true - автоматическое создание буферов глубины и трафарета
		d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8; // формат буферов глубины и трафарета
		d3dpp.Flags = 0; // доп. параметры
		d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT; // частота кадров(в данной ситуации по-умолчанию)
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE; // как отрисовывать(в данной ситуации пока выполнять немедленно)

		// Step 4: Create the device.
		HRESULT hr(0); // флаг, для проверок
		hr = d3d9->CreateDevice(D3DADAPTER_DEFAULT, deviceType, this->hwnd, vp, &d3dpp, &this->Device); //  создаём объект IDirect3DDevice9        
		 // физический объект по-умолчанию, тип устройства, дескриптор окна, флаг(хранит состояние отрисовки вершин), параметры устройства, указатель на созданое устройство
		if (FAILED(hr)) // если не удалось создать устройство
		{
			d3dpp.AutoDepthStencilFormat = D3DFMT_D16; // меняем формат буфера глубины и трафарета на 16-битный
			hr = d3d9->CreateDevice(D3DADAPTER_DEFAULT, deviceType, this->hwnd, vp, &d3dpp, &this->Device); // вновь создаём устройство
			if (FAILED(hr)) // если вновь не удалось создать
			{
				d3d9->Release(); // освобождаем память от информации, которая хранится(о видеокарте)
				MessageBox(0, L"CreateDevice() - FAILED", 0, 0); // выводим сообщение о неудаче
				return false; // возвращаем неудачу
			}
		}
		return true; // возвращаем истину
	}

	IDirect3DDevice9 * D3DXDevice::getDevice()
	{
		return this->Device;
	}

	D3DXDevice::~D3DXDevice()
	{
		d3d9->Release(); // освобождаем память от информации, которая хранится(о видеокарте) (больше не нужна)
		this->Device->Release(); // удаляем устройство
		std::for_each(models.begin(), models.end(), [](auto& e) {e.Free();});
	}

	void D3DXDevice::EnableLight(bool f)
	{
		HRESULT h = this->Device->SetRenderState(D3DRS_LIGHTING, f);
	}

	/* // ДОРАБОТАТЬ
	void D3DXDevice::reverseScreen(HWND hwnd, bool windowed, int color)
	{
		LPDIRECT3D9 q_d3d;
		D3DDISPLAYMODE disMode;
		d3d9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &disMode);

		ZeroMemory(&d3dpp, sizeof(d3dpp));
		d3dpp.Windowed = windowed;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.hDeviceWindow = hwnd;
		d3dpp.BackBufferWidth = disMode.Width; // this->height
		d3dpp.BackBufferHeight = disMode.Height; // this->wight
		d3dpp.BackBufferFormat = disMode.Format;
		d3dpp.BackBufferCount = 1;
		d3dpp.FullScreen_RefreshRateInHz = 0;
		d3dpp.EnableAutoDepthStencil = TRUE;
		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

		this->Device->Reset(&d3dpp);
		this->Display(color);
	}
	*/

	void D3DXDevice::SetMaterial(int num, const std::wstring& key)
	{
		this->Device->SetMaterial(&this->models[num].GetMaterialBuffer()[key].GetMaterial());
	}

	void D3DXDevice::SetEnableLight(int n, bool f)
	{
		if (f)
		{
			HRESULT h = Device->SetLight(n, &lights[n].GetLight());
			h = this->Device->SetRenderState(D3DRS_NORMALIZENORMALS, true); // ренормализация нормалей
			h = this->Device->SetRenderState(D3DRS_SPECULARENABLE, true); // поддержка отражаемого света
		}
		HRESULT h = Device->LightEnable(n, f);
	}

	void D3DXDevice::SetTexturesFilter(IDirect3DTexture9* texture)
	{
		Device->SetTexture(0, texture);
		Device->SetSamplerState(0, D3DSAMP_MAGFILTER, // качество
			D3DTEXF_LINEAR);
		Device->SetSamplerState(0, D3DSAMP_MINFILTER,
			D3DTEXF_LINEAR);
		//Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT); // автоматически работает(mip maps)
		Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP); // режим адресации(азполнения текстурой области)
		Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	}

	bool D3DXDevice::InitializationModel()
	{
		if (Device)
		{
			lights.push_back(D3DXLight(D3DLIGHT_POINT, Colors::White, Colors::White * 0.2f,
				Colors::White * 0.6f, D3DXVECTOR3(200, 200 , -200), 1000.f)); // создаём источник света
			for (const auto& name : files) // пробегаемся по всем путям до файла , и создаём на их основе модели
			{
				models.push_back({ name.c_str() }); // создаем модель + путь до модели(и материал)
				try
				{
					models.back().LoadModel(this->Device); // загружаем саму модель
				}
				catch (ErrorFile& er)
				{
					MessageBoxA(nullptr, "Error", er.what(), MB_OK);
					return false;
				}
			}

			// создаем матрицу вида(камеру)
			D3DXVECTOR3 p(0, 0, -2000); // z -2500
			D3DXVECTOR3 d(0, 0, 0);
			D3DXVECTOR3 h(0, 1, 0);
			D3DXMATRIX view;
			D3DXMatrixLookAtRH(&view, &p, &d, &h);
			Device->SetTransform(D3DTS_VIEW, &view);

			// матрица перспективной проекции
			D3DXMATRIX persp;
			RECT rect;
			GetWindowRect(this->hwnd, &rect);
			D3DXMatrixPerspectiveFovRH(&persp, gradus_to_radian(90), (rect.right - rect.left) / (rect.bottom - rect.top), 1.f, 3000.f);
			Device->SetTransform(D3DTS_PROJECTION, &persp);

			this->Device->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);    // включаем использование z-buffera
			this->Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
			this->EnableLight(false); // включаем свет
			this->SetEnableLight(0, true); // включаем нужный источник и режимы
			return true;
		}
		return false;
	}

	void D3DXDevice::ReadModelsCatalog(const wchar_t * name)
	{
		fs::path catalog(name);
		if (!fs::exists(catalog))
			throw std::system_error(std::error_code()); // пустое исключение, доработать потом
		fs::recursive_directory_iterator begin(name);
		fs::recursive_directory_iterator end;
		std::copy_if(begin, end, std::back_inserter(this->files), [](const fs::path& p) {
			return fs::is_regular_file(p) && p.extension() == L".obj"; // добавляем модели в контейнер, если они являются файлами
																	   // НЕ БИТЫМИ, и расширение файла - .obj
		});
	}

	void D3DXDevice::PaintModel()
	{
		if (this->Device)
		{
			D3DXMATRIX rotate;
			D3DXMatrixRotationY(&rotate, this->RotateY);
			D3DXMATRIX trans;
			D3DXMatrixTranslation(&trans, 0.f, 0.f, 0.f);
			rotate *= trans;

			HRESULT h = Device->Clear(NULL, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, Colors::Grey, 1, NULL);
			//Device->Clear(NULL, NULL, D3DCLEAR_ZBUFFER, Colors::White, 1, NULL);
			h = Device->BeginScene();
			h = Device->SetFVF(Vertex::FVF);
			Device->SetTransform(D3DTS_WORLD, &rotate);

			for (int i(0); i < this->models.size(); i++)
			{
				int pos(0);
				for (int j(0);j < this->models[i].GetVertexBufferData().size(); j++)
				{
						h = Device->SetStreamSource(0, this->models[i].GetBuffer(), 0, sizeof(Vertex)); 

						// выбирем буфер вершин
						std::wstring key(this->models[i].GetVertexBufferData()[j].first);

						D3DMATERIAL9 mtrl_file;
						mtrl_file.Ambient = Colors::White;
						mtrl_file.Diffuse = Colors::White;
						mtrl_file.Specular = Colors::White;
						mtrl_file.Emissive = Colors::Black;
						mtrl_file.Power = 5.f;
						//D3DMATERIAL9 mtrl_file = this->models[i].GetMaterialBuffer()[key].GetMaterial();
						h = this->Device->SetMaterial(&mtrl_file);

						// настройки текстур
						this->Device->SetTexture(0, this->models[i].GetTexturesBuffer()[key].GetTexture());
						this->SetTexturesFilter(this->models[i].GetTexturesBuffer()[key].GetTexture());

						//отрисовываем модель
						int CountPrimitive(this->models[i].GetVertexBufferData()[j].second); 
						h = Device->DrawPrimitive(D3DPT_TRIANGLELIST, pos, CountPrimitive);
						pos += (CountPrimitive * 3);
				}
			}
			Device->EndScene();
			Device->Present(NULL, NULL, NULL, NULL);
		}
	}

	//----------------------------------------------------------------------------------------------------//

	D3DXMaterial::D3DXMaterial()
	{
		this->material.Ambient = this->material.Diffuse = this->material.Emissive = Colors::Grey;
		this->material.Emissive = Colors::Black; // свечение, нет по-умолчанию
		this->material.Power = 5.f; // резкость зеркальных отражений
	}

	D3DXMaterial::D3DXMaterial(const D3DXCOLOR & c, const D3DXCOLOR& e, float p)
	{
		this->material.Ambient = this->material.Diffuse = this->material.Specular = c;
		this->material.Emissive = e; // свечение, нет по-умолчанию
		this->material.Power = p; // резкость зеркальных отражений
	}

	D3DXMaterial::D3DXMaterial(const D3DXCOLOR & a, const D3DXCOLOR & d, const D3DXCOLOR& s, const D3DXCOLOR & e, float p)
	{
		this->material.Ambient = a;
		this->material.Diffuse = d;
		this->material.Specular = s;
		this->material.Emissive = e; // свечение, нет по-умолчанию
		this->material.Power = p; // резкость зеркальных отражений
	}

	D3DXMaterial::D3DXMaterial(const D3DXMaterial & m)
	{
		if (this != &m)
			this->material = m.material;
	}

	D3DXMaterial & D3DXMaterial::operator=(const D3DXMaterial & m)
	{
		if (this != &m)
			this->material = m.material;
		return *this;
	}

	D3DMATERIAL9 D3DXMaterial::GetMaterial()
	{
		return this->material;
	}

	// Colors
	const D3DXCOLOR Colors::Red(D3DCOLOR_XRGB(255, 0, 0));
	const D3DXCOLOR Colors::White(D3DCOLOR_XRGB(255, 255, 255));
	const D3DXCOLOR Colors::Black(D3DCOLOR_XRGB(0, 0, 0));
	const D3DXCOLOR Colors::Blue(D3DCOLOR_XRGB(0, 0, 255));
	const D3DXCOLOR Colors::Grey(D3DCOLOR_XRGB(128, 128, 128));
	const D3DXCOLOR Colors::Yellow(D3DCOLOR_XRGB(255, 255, 0));
	//

	//-------------------------------------------------------------------------------------------------//
	D3DXLight::D3DXLight()
	{
		ZeroMemory(&this->light, sizeof(D3DLIGHT9));
		this->light.Type = D3DLIGHT_POINT;
		this->light.Diffuse = Colors::White;
		this->light.Specular = Colors::White * 0.3f;
		this->light.Ambient = Colors::White * 0.6f;
		this->light.Position = D3DXVECTOR3(0.f, 20.f, 0.f);
		this->light.Range = 1000.f;
	}

	D3DXLight::D3DXLight(D3DLIGHTTYPE t, const D3DXCOLOR & d, const D3DXCOLOR & s, const D3DXCOLOR & a, const D3DXVECTOR3 & v, float r)
	{
		ZeroMemory(&this->light, sizeof(D3DLIGHT9));
		this->light.Type = t;
		this->light.Diffuse = d;
		this->light.Specular = s;
		this->light.Ambient = a;
		this->light.Position = v;
		this->light.Range = r;
	}

	D3DXLight::D3DXLight(D3DLIGHTTYPE t, const D3DXCOLOR & d, const D3DXCOLOR & s, const D3DXCOLOR & a, const D3DXVECTOR3 & v)
	{
		ZeroMemory(&this->light, sizeof(D3DLIGHT9));
		this->light.Type = t;
		this->light.Diffuse = d;
		this->light.Specular = s;
		this->light.Ambient = a;
		this->light.Position = v;
	}

	D3DXLight::D3DXLight(const D3DXLight & l)
	{
		if (this != &l)
			this->light = l.light;
	}

	D3DXLight & D3DXLight::operator=(const D3DXLight & l)
	{
		if (this != &l)
			this->light = l.light;
		return *this;
	}

	D3DLIGHT9 & D3DXLight::GetLight()
	{
		return this->light;
	}

	//--------------------------------------------------------------------------------------------------------------//

	bool D3DXTexture::LoadFromFile(IDirect3DDevice9 * dev, const char * name_file)
	{
		HRESULT hResult(D3DXCreateTextureFromFileA(dev, name_file, &this->texture));
		if (hResult == S_OK)
			return true;
		return false;
	}

	D3DXTexture::D3DXTexture(const D3DXTexture & t)
	{
		this->texture = t.texture;
	}

	D3DXTexture & D3DXTexture::operator=(const D3DXTexture & t)
	{
		if (this != &t)
		{
			if (t.texture)
				this->texture = t.texture;
		}
		return *this;
	}

	D3DXTexture::~D3DXTexture()
	{
		this->texture = nullptr; // в деструкторе просто обнуляем ссылку на текстуру
	}

	void D3DXTexture::FreeTexture()
	{
		if (this->texture)
			this->texture->Release();
	}

	//------------------------------------------------------------------------------------------------------------------------//

	const char * ErrorFile::what() const
	{
		return this->message.c_str();
	}

	//------------------------------------------------------------------------------------------------------------------------//
	void D3DModel::LoadModel(IDirect3DDevice9* dev)
	{
		std::wifstream file_model(this->FileName);
		if (!file_model)
			throw ErrorFile("Can't open file\n");

		std::map<std::wstring, int> select{ { L"v ", 0 },{ L"vt", 1 },{ L"vn", 2 },{ L"f ", 3 } };
		std::wstring temp;
		std::wstring CurrentNameTexture;

		std::vector<std::pair< std::wstring, std::vector<Index> > > indBuf;
		std::vector<D3DXVECTOR3> verBuf; // вершины
		std::vector<D3DXVECTOR3> normBuf; // нормали
		std::vector<D2VTexture> texturesBuf; // коорд. текстуры

		// считываем данные модели
		while (true)
		{
			std::getline(file_model, temp, L'\n');
			if (file_model.eof())
				break;
			if (!file_model)
				throw ErrorFile("Error read file\n");
			if (!temp.find(L"usemtl")) // если встретился новый материал, то это новый индекс буффер
			{
				CurrentNameTexture = { temp.begin() + 7, temp.end() };
				textures[CurrentNameTexture] = D3DXTexture(); // добавляем в карту текстуру
				indBuf.push_back({ CurrentNameTexture, std::vector<Index>() }); // добавляем новый индекс буфер с именем текстуры
				continue;
			}

			try
			{
				std::wstring number;
				std::vector<float> tmp;
				std::wstring sel;
				int ind[3];

				if (temp.length() >= 2)
					sel = temp.substr(0, 2);
				else
					sel = temp;
				switch (select.at(sel))
				{
				case 0: // вершины
				{
					temp += L" ";
					temp.erase(0, 2);
					std::for_each(temp.begin(), temp.end(), [&number, &tmp](const wchar_t& e) { // итератор вылетает за границы
						if (e != L' ')
							number += e;
						else
						{
							tmp.push_back(0.f);
							std::wstringstream ostream(number);
							ostream >> tmp.back();
							number.clear();
						}
					});
					// переворачиваем отзеркаленную модель
					D3DXVECTOR3 vect(tmp[0], tmp[1], tmp[2]);
					/*D3DXMATRIX mirror;
					D3DXMatrixTranslation(&mirror, 0, 0, 0);
					mirror(2, 2) = -1;
					D3DXVec3TransformCoord(&vect, &vect, &mirror);*/
					// добавляем в буфер вершин
					verBuf.push_back(vect);
					break;
				}

				case 1: // текстуры
				{
					temp += L" ";
					temp.erase(0, 3);
					std::for_each(temp.begin(), temp.end(), [&number, &tmp](const wchar_t& e) {
						if (e != L' ')
							number += e;
						else
						{
							tmp.push_back(0.f);
							std::wstringstream ostream(number);
							ostream >> tmp.back();
							number.clear();
						}
					});
					texturesBuf.push_back({ tmp[0], 1 - tmp[1] });
					break;
				}

				case 2: // нормали
				{
					temp += L" ";
					temp.erase(0, 3);
					std::for_each(temp.begin(), temp.end(), [&number, &tmp](const wchar_t& e) {
						if (e != L' ')
							number += e;
						else
						{
							tmp.push_back(0.f);
							std::wstringstream ostream(number);
							ostream >> tmp.back();
							number.clear();
						}
					});
					normBuf.push_back({ tmp[0], tmp[1], tmp[2] });
					break;
				}

				case 3: // фейсы(индексы)
				{
					std::vector<int> Tmp;
					temp += L" ";
					temp.erase(0, 2);
					std::for_each(temp.begin(), temp.end(), [&number, &Tmp](const wchar_t& e) {
						if (e != L'/' && e !=L' ')
							number += e;
						else
						{
							Tmp.push_back(0.f);
							std::wstringstream ostream(number);
							ostream >> Tmp.back();
							Tmp.back()--;
							number.clear();
						}
					});
					Tmp.back()--;

					for (int i(0); i < Tmp.size(); i += 3)
						indBuf.back().second.push_back({ Tmp[i], Tmp[i + 1], Tmp[i + 2] });
					//std::swap(indBuf.back().second.back(), indBuf.back().second[indBuf.back().second.size() - 3]); // меняем индексы местами дабы не отзеркаливало
					break;
				}

				default:
					break;
				}
			}
			catch (std::out_of_range&) // заглушка
			{
			}
		}
		file_model.close();

		std::wifstream file_material(std::wstring({this->FileName.begin(), this->FileName.end() - 3}) + L"mtl");
		if (!file_material)
			throw ErrorFile("Can't open file\n");

		// считываем все текстуры и материалы
		std::wstringstream val;
		while (true)
		{
			std::getline(file_material, temp, L'\n');
			if (file_material.eof())
				break;
			if (!file_material)
				throw ErrorFile("Error read file\n");
			if (!temp.find(L"newmtl"))
			{
				// название материала
				std::wstring search(temp.begin() + 7, temp.end());

				// резкость
				std::getline(file_material, temp, L'\n'); 
				temp.erase(0, 6); val << temp; float power(0.f); val >> power;
				val.clear();

				// настройки материала
				std::vector<D3DXCOLOR> mat;
				for (int i(0); i < 3; i++) // получаем значения света(три параметра: ambient, specular, diffuse)
				{
					std::getline(file_material, temp, L'\n');
					temp.erase(0, 3);
					std::vector<std::wstring> values;
					std::wstring tmp;
					std::for_each(temp.begin(), temp.end(), [&values, &tmp](const char& e) {
						if (e == L' ' || e == L'\0')
						{
							values.push_back(tmp);
							tmp.clear();
						}
						else
							tmp += e;
					});
					values.push_back(tmp);
					tmp.clear();

					std::vector<float> values_final;
					std::for_each(values.begin(), values.end(), [&values_final](const std::wstring& e) {
						std::wstringstream converter;
						converter << e;
						values_final.push_back(0.f);
						converter >> values_final.back();
					});

					mat.push_back(D3DXCOLOR(values_final[1], values_final[0], values_final[2], 0.f));
				}

				// настройки текстуры
				std::getline(file_material, temp, L'\n');
				if (temp.find(L"map_Kd "))
					throw ErrorFile("Error read file\n");
				//создаем материал
				D3DXTexture _texture;
				std::string name(temp.begin() + 7, temp.end()); name.insert(0, "model\\");
				_texture.LoadFromFile(dev, name.c_str());
				textures[search] = _texture;


				// свечение
				std::getline(file_material, temp, L'\n'); 
				temp.erase(0, 3);
				val << temp; float emmisive(0.f); val >> emmisive;
				val.clear();
				// создаем материал
				material[search] = D3DXMaterial(mat[0], mat[1], mat[2], D3DXCOLOR(emmisive, emmisive, emmisive, 1.0f), power);
			}
			else
				throw ErrorFile("Error read file\n");
		}

		file_material.close();

		// создаем нужные нам буферы
		int size(0);
		std::for_each(indBuf.begin(), indBuf.end(), [&size](const auto& e)
		{
			size += e.second.size();
		});
		dev->CreateVertexBuffer(size * sizeof(Vertex), D3DUSAGE_WRITEONLY, Vertex::FVF, D3DPOOL_MANAGED,
			&this->buffer, NULL);
		Vertex* v(nullptr);
		this->buffer->Lock(0, 0, (void**)&v, NULL);
		int i(0);

		for (const auto& elem : indBuf)
		{
			for (const auto& e : elem.second)
			{
				Vertex ver = Vertex(verBuf[e.v], texturesBuf[e.t], normBuf[e.n]);
				v[i++] = ver;
			}

			vB.push_back({ elem.first, elem.second.size()/3 });
		}
		this->buffer->Unlock();
	}

	void D3DModel::Free()
	{
		/*std::for_each(vB.begin(), vB.end(), [](auto& e) {
			if (e.second)
				e.second->Release();
		});*/
		this->buffer->Release();
		std::for_each(textures.begin(), textures.end(), [](auto& e) {
			if (e.second.GetTexture())
				e.second.FreeTexture();
		});
	}

	//---------------------------------------------------------------------------------//
	FLOAT gradus_to_radian(FLOAT gradus)
	{
		return gradus * (D3DX_PI / 180);
	}

	//-----------------------------------------------------------------------------------//
	std::ofstream & operator<<(std::ofstream & f, const Vertex & v)
	{
		f << "Vertex: " << v.x << ", " << v.y << ", " << v.z << "; normal: " << v.nx << ", " << v.ny << ", " << v.nz << "; texture: " <<
			v.u << ", " << v.v;
		return f;
	}

	const DWORD Vertex::FVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

};

// метод парсинга файла - переписать, используя std::algorithm(еще переписать несколько блоков)
// задать качество отрисовываемой текстуры

// изменить в парсере отлов исключения на что-то другое
// баг с освещением и моделью, TODO